<?php
error_reporting(E_ERROR | E_CORE_ERROR | E_RECOVERABLE_ERROR | E_COMPILE_ERROR);

spl_autoload_register(function ($class_name) {
    include __DIR__.'/src/'.$class_name . '.php';
});

require __DIR__.'/vendor/autoload.php';

