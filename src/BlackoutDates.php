<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 6/1/16
 * Time: 11:03 AM
 */

namespace Selectitaly\BlackoutDates;

use \DateTime;

/**
 * Class BlackoutDates
 *
 * Is in charge of returning all dates that need to be blacked out to indicate that business requests of certain types
 * can't be guaranteed to be completed by.
 *
 * To use this class, instantiate it, use the setCustomBlackoutDatesArray() method to import any required custom
 * blackout dates, and then call the getAll() method.
 *
 * To change all 12pm references (after which time Tomorrow is blacked out), pass a date('G') compatible integer to
 * the getBlackoutTomorrowAfterThisHour() method.
 *
 *
 * Blackout dates are automatically created for:
 *      - Today
 *      - if after 12pm: Tomorrow
 *      - if Friday after 12pm through Sunday: Saturday, Sunday and Monday
 *
 * @package app\Classes
 */
class BlackoutDates
{
    /**
     * @var string[] Array of "Y-m-d H:i:s" keys which indicate the date & time
     *               to start blacking out the elements of that key's sub-array
     *               which are also of the same "Y-m-d H:i:s" format
     *
     *
     *               example: // July 4th 2016
     *                      [
     *                          '2016-07-01 12:00:00' => [
     *                              '2016-07-04 23:59:59',
     *                              '2016-07-05 23:59:59'
     *                          ]
     *                      ]
     */
    private $customBlackoutDatesArray = [];
    private $dateFormatOfArrayKeys = 'm-d-y';
    private $blackoutTomorrowAfterThisHour = 12;

    /** @var DateTime Equals Now */
    private $todaysDate;

    /**
     * Returns an associative array of all blackout. The array elements are in the given $dateFormat.
     * The keys are the same date but in the 'm-d-y' format.
     *
     * @param string $dateFormat
     *
     * @return \string[]
     */
    public function getAll($dateFormat = 'Ymd'){
        return $this->getNormal($dateFormat) + $this->getCustom($dateFormat);
    }

    /**
     * Creates blackout dates for:
     *      - if after 12pm: Tomorrow
     *      - if Friday after 12pm through Sunday: Saturday, Sunday and Monday
     *
     * @param string $dateFormat
     *
     * @return string[] Returns an associative array of blackout dates.
     * The returned associated array's keys are always of format 'm-d-y' and the elements are of the format specified
     * by the $dateFormat input
     */
    public function getNormal($dateFormat = 'Ymd'){

        $now = $this->getTodaysDate();
        $normalDates = [];

        // Friday after Noon
        if($now->format('w') == 5 && $now->format('G') >= $this->getBlackoutTomorrowAfterThisHour())
        {
            $normalDates += $this->getXDaysFromTodayArray([0,1,2,3], $dateFormat);
        }
        // Saturday
        elseif($now->format('w') == 6)
        {
            $normalDates += $this->getXDaysFromTodayArray([0,1,2], $dateFormat);
        }
        // Sunday
        elseif($now->format('w') == 7 || $now->format('w') == 0)
        {
            $normalDates += $this->getXDaysFromTodayArray([0,1], $dateFormat);
        }
        // Any day after Noon
        elseif($now->format('G') >= $this->getBlackoutTomorrowAfterThisHour())
        {
            $normalDates += $this->getXDaysFromTodayArray([0,1], $dateFormat);
        }
        // today is always blocked off
        else{
            $normalDates += $this->getXDaysFromTodayArray([0], $dateFormat);
        }

        return $normalDates;
    }

    /**
     * Runs getXDaysFromToday() on each number in the provided int array and returns an associative array of the
     * resulting dates.
     *
     * The returned associated array's keys are always of format 'm-d-y' and the elements are of the format specified
     * by the $dateFormat input
     *
     * @param int[] $numberOfDaysArray
     * @param string $dateFormat
     *
     * @return string[]
     */
    private function getXDaysFromTodayArray($numberOfDaysArray, $dateFormat = 'Ymd'){
        $dateArray = [];
        foreach($numberOfDaysArray as $numberOfDays){
            $dateArray[$this->getXDaysFromToday($numberOfDays,$this->dateFormatOfArrayKeys)] = $this->getXDaysFromToday($numberOfDays,$dateFormat);
        }
        return $dateArray;
    }

    /**
     * Returns a sting in the specified $dateFormat that is $numberOfDays in the future
     *
     * @param int    $numberOfDays
     * @param string $dateFormat
     *
     * @return string
     */
    private function getXDaysFromToday($numberOfDays, $dateFormat){
        // the clone keyword is required so that the date object getting returned by the getTodaysDate() function is not modified directly
        $today = clone $this->getTodaysDate();
        if($numberOfDays > 0){
            $today->add(new \DateInterval('P'.$numberOfDays.'D'));
        }
        return $today->format($dateFormat);
    }

    /**
     * Returns all relevant custom blackout dates via an associated array of date strings where:
     * The returned associated array's keys are always of format 'm-d-y' and the elements are of the format specified
     * by the $dateFormat input
     *
     * @param string $dateFormat (Default = 'Ymd') Optional
     *
     * @return array
     */
    public function getCustom($dateFormat = 'Ymd'){
        $now = $this->getTodaysDate();
        $customDates = [];

        foreach($this->getCustomBlackoutDatesArray() as $blackoutStartDate => $datesToBlackout){
            $blackoutStartDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $blackoutStartDate);
            if($now >= $blackoutStartDateTime){
                $customDates += $this->getFutureDatesFromArray($datesToBlackout, $dateFormat);
            }
        }

        return $customDates;
    }

    /**
     * @return \string[][]
     */
    private function getCustomBlackoutDatesArray()
    {
        return $this->customBlackoutDatesArray;
    }

    /**
     * Returns a DateTime object that is NOW
     *
     * @return DateTime
     */
    public function getTodaysDate(){
        if(!$this->todaysDate){
            $this->todaysDate = new DateTime();
        }

        return $this->todaysDate;
    }


    /**
     * Accepts an array of 'Y-m-d H:i:s' strings and returns an associated array of the same dates consisting
     * of only the dates that are still in the future.
     *
     * The returned associated array's keys are always of format 'm-d-y' and the keys are of the format specified
     * by the $dateFormat input
     *
     * @param string[] $datesArray Array of 'Y-m-d H:i:s' date strings
     * @param string $dateFormat (Default = 'Ymd') Optional.
     *
     * @return array
     */
    private function getFutureDatesFromArray($datesArray, $dateFormat = 'Ymd'){
        $now = $this->getTodaysDate();
        $futureDates = [];

        foreach($datesArray as $dateString){
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateString);
            if($now <= $date){
                $futureDates[$date->format($this->dateFormatOfArrayKeys)] = $date->format($dateFormat);
            }
        }
        return $futureDates;
    }

    /**
     * Sets the customBlackoutDatesArray as long as the input is not null
     *
     * @param \string[] $customBlackoutDatesArray
     */
    public function setCustomBlackoutDatesArray($customBlackoutDatesArray = null)
    {
        if(is_array($customBlackoutDatesArray)){
            $this->customBlackoutDatesArray = $customBlackoutDatesArray;
        }
    }

    /**
     * @return int
     */
    private function getBlackoutTomorrowAfterThisHour()
    {
        return $this->blackoutTomorrowAfterThisHour;
    }

    /**
     * @param int $blackoutTomorrowAfterThisHour Integer between 0 & 23 (military time | date('G'))
     */
    public function setBlackoutTomorrowAfterThisHour($blackoutTomorrowAfterThisHour)
    {
        $this->blackoutTomorrowAfterThisHour = intval($blackoutTomorrowAfterThisHour);
    }
}