#  Select Italy - Blackout Dates

### Description of Blackout Dates:

When a company receives a request, if clients can specify the date by which the request should be fulfilled by,
that company will likely have to restrict (blackout) some dates from the client's available choices based on
office hours of available personnel. This class generates these blackout dates as an associative array where the
keys are of format 'm-d-y' and the values are of format 'Ymd' (by default).


### To use the BlackoutDates class:

1. Instantiate the BlackoutDates Class
2. If custom dates need to be blacked out, pass use the setCustomBlackoutDatesArray() method
3. If you need to change the time of day for clients should not be able to select tomorrow's date, use the 
   setBlackoutTomorrowAfterThisHour() method
4. Get all appropriate blackout dates for the current date & time by calling the getAll() method
5. Use the resulting array to somehow blackout dates in your application (for example, to disable dates on a 
   Bootstrap Datepicker calendar see
   http://bootstrap-datepicker.readthedocs.io/en/latest/methods.html?highlight=datesDisabled#setdatesdisabled )


### How the BlackoutDates class calculates normal blackout dates:

* The current day is always blacked out
* If it's 12:00 or later (using military time), tomorrow is blacked out (see Option 1)
* If tomorrow is to be blacked out, but tomorrow is a weekend day, then all subsequent weekend days and the next business day are blacked out
* If today is a weekend day, Monday is blacked out

If these requirements don't suit you and you need to further customize the normal blackout date logic, override 
the getNormal() method and implement your own logic, or request an update to this library! 

### Options

1. To change the time of day at which tomorrow should be blacked out, pass an integer between 1 & 24 representing 
   date('G') to the setBlackoutTomorrowAfterThisHour() method.
2. To specify an alternate date format for the values of the returned associative date array from the default 'Ymd' format, 
   pass in an alternate date format to the getAll() method.
3. To blackout holidays or other days that might not be business days, pass an array consisting of date("Y-m-d H:i:s") 
   compatible string elements to the setCustomBlackoutDatesArray() method. Here's an example that would black out 
   Monday July 4th and 5th after 12:00 on July 1st 2016. (since July 4th 2016 is a Monday, it actually doesn't 
   have to get included here since it will be blacked out normally, but it makes the array easier to understand)
```    
    [
        // July 4th 2016
        '2016-07-01 12:00:00' => [
            '2016-07-04 23:59:59',
            '2016-07-05 23:59:59'
    ],
```
