<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 6/3/16
 * Time: 10:40 AM
 */

namespace Selectitaly\BlackoutDates\Tests;
use phpunit\framework\TestCase as PhpUnitTestCase;

class TestCase  extends PhpUnitTestCase
{

    protected static function callMethod($classObject, $methodName, array $arguments) {
        $class = new \ReflectionClass($classObject);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($classObject, $arguments);
    }
}