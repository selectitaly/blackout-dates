<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 6/1/16
 * Time: 12:37 PM
 */

namespace Selectitaly\BlackoutDates\Tests;

use Selectitaly\BlackoutDates\BlackoutDates;

class BlackoutDatesTest extends TestCase{



    /**
     * @test
     */
    public function custom_blackout_dates_array_is_properly_formatted(){
        $blackoutDatesObject = new BlackoutDates();
        $datesArray = self::callMethod($blackoutDatesObject, 'getCustomBlackoutDatesArray', []);

        foreach($datesArray as $dateKey => $dateSubArray){
            $this->assertNotFalse((\DateTime::createFromFormat("Y-m-d H:i:s", $dateKey))?true:false);
            foreach($dateSubArray as $dateString){
                $this->assertNotFalse((\DateTime::createFromFormat("Y-m-d H:i:s", $dateString))?true:false);
            }
        }
    }

    /**
     * @test
     */
    public function get_all_blackout_dates_calls_normal_and_custom_methods(){

        /** @var \PHPUnit_Framework_MockObject_MockObject|BlackoutDates $blackoutStub */
        $blackoutStub = $this->getMockBuilder('Selectitaly\BlackoutDates\BlackoutDates')
            ->setMethods(['getNormal','getCustom'])
            ->getMock();
        $blackoutStub->method('getNormal')
                ->willReturn(['a'=>1]);
        $blackoutStub->method('getCustom')
                ->willReturn(['b'=>2]);

        $this->assertEquals(['a'=>1,'b'=>2],$blackoutStub->getAll());
    }

    /**
     * Creates a Mock of the Blackout class with the getTodaysDate() method overridden so that a
     * specific date can be returned
     *
     * @param $dateTime
     *
     * @return \PHPUnit_Framework_MockObject_MockObject|BlackoutDates
     */
    private function getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($dateTime){
        /** @var \PHPUnit_Framework_MockObject_MockObject|BlackoutDates $blackoutStub */
        $blackoutStub = $this->getMockBuilder('Selectitaly\BlackoutDates\BlackoutDates')
            ->setMethods(['getTodaysDate'])
            ->getMock();
        $blackoutStub->method('getTodaysDate')
            ->willReturn($dateTime);

        $blackoutStub->setCustomBlackoutDatesArray(
        // July 4th 2016
            [
                '2016-07-01 12:00:00' => [
                    '2016-07-04 23:59:59',
                    '2016-07-05 23:59:59'
                ]
            ]
        );

        return clone $blackoutStub;
    }

    /**
     * @test
     */
    public function blackout_dates_for_friday_morning_only_today(){
        $fridayMorning = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 11:00:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayMorning);

        $this->assertEquals(['07-01-16' => '20160701'],$blackoutStub->getNormal());
    }

    /**
     * @test
     */
    public function blackout_dates_for_friday_afternoon_are_friday_through_monday(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 13:01:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-01-16' => '20160701',
                '07-02-16' => '20160702',
                '07-03-16' => '20160703',
                '07-04-16' => '20160704'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_friday_at_noon_are_friday_through_monday(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 12:00:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-01-16' => '20160701',
                '07-02-16' => '20160702',
                '07-03-16' => '20160703',
                '07-04-16' => '20160704'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_friday_custom_early_hour_are_tomorrow(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 08:01:03');


        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);
        $blackoutStub->setBlackoutTomorrowAfterThisHour(9);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-01-16' => '20160701'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_saturday_are_saturday_through_monday(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-02 13:01:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-02-16' => '20160702',
                '07-03-16' => '20160703',
                '07-04-16' => '20160704'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_sunday_are_sunday_through_monday(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-03 13:01:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-03-16' => '20160703',
                '07-04-16' => '20160704'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_any_afternoon_are_that_day_through_tomorrow(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-06 13:01:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-06-16' => '20160706',
                '07-07-16' => '20160707'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_any_day_at_noon_are_that_day_through_tomorrow(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-06 12:00:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $blackoutDates = $blackoutStub->getNormal();
        $this->assertEquals(
            [
                '07-06-16' => '20160706',
                '07-07-16' => '20160707'
            ],
            $blackoutDates);
    }

    /**
     * @test
     */
    public function blackout_dates_for_any_morning_only_today(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-06 09:00:00');

        $blackoutStub = $this->getBlackoutDatesMockWithTodaysDateMethodReturningGivenDate($fridayAfternoon);

        $this->assertEquals(['07-06-16' => '20160706'],$blackoutStub->getNormal());
    }


    /**
     * Creates a Mock of the Blackout class specifically for the the tests that check the custom dates method
     *
     * @param $todaysDate
     *
     * @return \PHPUnit_Framework_MockObject_MockObject|BlackoutDates
     */
    private function getStubForCustomDatesChecking($todaysDate){
        /** @var \PHPUnit_Framework_MockObject_MockObject|BlackoutDates $blackoutStub */
        $blackoutStub = $this->getMockBuilder('Selectitaly\BlackoutDates\BlackoutDates')
            ->setMethods(['getTodaysDate'])
            ->getMock();
        $blackoutStub->method('getTodaysDate')
            ->willReturn($todaysDate);
        $blackoutStub->setCustomBlackoutDatesArray(
            [
                // July 4th 2016
                '2016-07-01 12:00:00' => [
                    '2016-07-04 23:59:59',
                    '2016-07-05 23:59:59'
                ],
                // July 4th 2017
                '2017-07-03 12:00:00' => [
                    '2017-07-04 23:59:59',
                    '2017-07-05 23:59:59'
                ]
            ]
        );

        return $blackoutStub;
    }

    /**
     * @test
     */
    public function get_custom_blackout_dates_returns_nothing_for_non_relevant_early_dates(){
        $fridayMorning = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 11:01:00');

        $blackoutStub = $this->getStubForCustomDatesChecking($fridayMorning);


        $this->assertEmpty($blackoutStub->getCustom());
    }

    /**
     * @test
     */
    public function get_custom_blackout_dates_returns_nothing_for_non_relevant_late_dates(){
        $nonRelevantDate = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-16 11:00:00');

        $blackoutStub = $this->getStubForCustomDatesChecking($nonRelevantDate);


        $this->assertEmpty($blackoutStub->getCustom());
    }

    /**
     * @test
     */
    public function get_custom_blackout_dates_for_first_relevant_date_in_afternoon(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 13:01:00');

        $blackoutStub = $this->getStubForCustomDatesChecking($fridayAfternoon);

        $this->assertEquals(['07-04-16' => '20160704','07-05-16' => '20160705'],$blackoutStub->getCustom());
    }

    /**
     * @test
     */
    public function get_custom_blackout_dates_for_relevant_dates_at_noon(){
        $fridayAfternoon = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-01 12:00:00');

        $blackoutStub = $this->getStubForCustomDatesChecking($fridayAfternoon);

        $this->assertEquals(['07-04-16' => '20160704','07-05-16' => '20160705'],$blackoutStub->getCustom());
    }

    /**
     * @test
     */
    public function get_custom_blackout_dates_for_relevant_dates(){
        $sunday = \DateTime::createFromFormat('Y-m-d H:i:s','2016-07-03 15:39:16');

        $blackoutStub = $this->getStubForCustomDatesChecking($sunday);

        $this->assertEquals(['07-04-16' => '20160704','07-05-16' => '20160705'],$blackoutStub->getCustom());
    }

}